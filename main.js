import { app, BrowserWindow } from 'electron'
import * as path from 'node:path'
import { fileURLToPath } from 'node:url'

// filename & dirname
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)


// Create the browser window
const createWindow = () => {
	const window = new BrowserWindow({
		width: 960,
		height: 720,
		// webPreferences: {

		// }
	})

	window.loadFile("index.html")
}

app.whenReady().then(() => {
	// Fenster erstllen
	createWindow()

	app.on("activate", () => {
		if (BrowserWindow.getAllWindows().length === 0) createWindow()
	})
})

app.on("window-all-closed", () => {
	if (process.platform !== "darwin") app.quit()
})
